package com.mycompany.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Player {
    // An ID which no player has
    public static final int NONE_ID = -1;
    private int id;
    private int position;
    private int money;
    private boolean isInJail;
    private boolean hasMustMoveCard;
    private boolean hasFreeOutOfJail;

    Player(int id, int position, int money) {
        this.id = id;
        this.position = position;
        this.money = money;
        this.isInJail = false;
        this.hasMustMoveCard = false;
        this.hasFreeOutOfJail = false;
    }

    public int getID() {
        return id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public boolean getIsInJail() {
        return isInJail;
    }

    public void setIsInJail(boolean isInJail) {
        this.isInJail = isInJail;
    }

    public boolean getHasMustMoveCard() {
        return hasMustMoveCard;
    }

    public void setHasMustMoveCard(boolean moveCard) {
        this.hasMustMoveCard = moveCard;
    }

    public boolean getHasFreeOutOfJail() {
        return hasFreeOutOfJail;
    }

    public void setHasFreeOutOfJail(boolean jailCard) {
        this.hasFreeOutOfJail = jailCard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return id == player.id &&
                getPosition() == player.getPosition() &&
                getMoney() == player.getMoney() &&
                isInJail == player.isInJail &&
                getHasMustMoveCard() == player.getHasMustMoveCard() &&
                getHasFreeOutOfJail() == player.getHasFreeOutOfJail();
    }

    public enum Names {
        NONE("Ingen"),  // Id -1
        DOG("Hunden"),  // Id 0
        CAT("Katten"),  // Id 1
        CAR("Bilen"),   // Id 2
        SHIP("Skibet"); // Id 3
        private String name;
        Names(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    public enum IDs {
        NONE(-1), // Id -1
        DOG(0),   // Id 0
        CAT(1),   // Id 1
        CAR(2),   // Id 2
        SHIP(3);  // Id 3
        private int id;
        IDs(int n) {
            this.id = n;
        }

        public int getID() {
            return id;
        }
    }

}
