package com.mycompany.app;

import junit.framework.TestCase;

import java.util.List;

public class LogicTest extends TestCase {

    public void testGetSingleton() {
        assertNotNull(Logic.getSingleton());
    }

    public void testReset() {
        Logic logic = Logic.getSingleton();
        logic.reset(4);
        List<Field> field = logic.getBoard();
    }

    public void testShuffle() {
    }

    public void testRollDie() {
        Logic logic = Logic.getSingleton();

        boolean dieBetweenOneAndSix = true;

        for (int i = 0; i < 1000; i++){
            dieBetweenOneAndSix = dieBetweenOneAndSix && logic.rollDie() <= 6 && logic.rollDie() >= 1;
        }
        assertTrue(dieBetweenOneAndSix);
    }

    public void testMovePlayer() {

    }

    public void testApplyConsequences() {
        Logic logic = Logic.getSingleton();
        logic.reset(2);

        // Test that it contains START_SUM and GO_TO_JAIL, and only processes GO_TO_JAIL.
        logic.startNextTurn(); //0
        logic.movePlayer(18);
        List<Logic.PlayerConsequence> consequences = logic.getConsequenceOfMove(22);

        int currentMoney = logic.getPlayers().get(0).getMoney();
        assertTrue(logic.applyConsequences(consequences) == null);
        int afterMoney = logic.getPlayers().get(0).getMoney();
        assertEquals(currentMoney, afterMoney);

        // Test that if there is a CHANCE-consequence, it's not returning null
        logic.reset(2);
        logic.startNextTurn(); //0
        logic.movePlayer(21);
        consequences = logic.getConsequenceOfMove(0);

        assertTrue(logic.applyConsequences(consequences) != null);

        // Test MUST_BUY (Player 1 buys property at Board.get(1))

        logic.startNextTurn();
        logic.movePlayer(1);
        consequences = logic.getConsequenceOfMove(0);
        currentMoney = logic.getPlayers().get(1).getMoney();
        logic.applyConsequences(consequences);
        afterMoney = logic.getPlayers().get(1).getMoney();

        assertTrue(currentMoney > afterMoney);

        // Test MUST_PAY_SINGLY (Player 0 steps on Board.get(1), which is owned by player 1)

        logic.reset(2);
        Property field1 = (Property) logic.getBoard().get(1);
        logic.startNextTurn(); //0
        logic.startNextTurn(); //1
        logic.movePlayer(1);
        logic.applyConsequences(logic.getConsequenceOfMove(0));

        logic.startNextTurn(); //0

        logic.movePlayer(1);
        consequences = logic.getConsequenceOfMove(0);

        currentMoney = logic.getPlayers().get(0).getMoney();
        logic.applyConsequences(consequences);
        afterMoney = logic.getPlayers().get(0).getMoney();

        assertEquals(afterMoney, currentMoney - field1.getPrice());

        // Test MUST_PAY_DOUBLY. (Player 1 already owns Board.get(1), now we buy the adjacent property and lets
        // Player 0 step on the field again)

        logic.startNextTurn(); //1
        consequences = logic.getConsequenceOfMove(0);
        logic.applyConsequences(consequences);
        logic.movePlayer(1);
        consequences = logic.getConsequenceOfMove(1);
        logic.applyConsequences(consequences);

        logic.startNextTurn(); //0
        logic.movePlayer(1);
        consequences = logic.getConsequenceOfMove(0);
        assertTrue(consequences.contains(Logic.PlayerConsequence.MUST_PAY_DOUBLY));
        currentMoney = logic.getPlayers().get(0).getMoney();
        logic.applyConsequences(consequences);
        afterMoney = logic.getPlayers().get(0).getMoney();

        assertEquals(afterMoney, currentMoney - field1.getPrice()*2);

    }

    public void testProcessChanceCard() {
    }


    public void testDrawCard() {
    }

    public void testGetPlayers() {
        Logic logic = Logic.getSingleton();
        logic.reset(4);
        assertTrue(logic.getPlayers().size() == 4);
    }

    public void testGetBoard() {
        Logic logic = Logic.getSingleton();
        logic.reset(4);
        List<Field> board = Logic.getSingleton().getBoard();
        assertNotNull(board);
        assertEquals(board.get(0).getType(), Field.Type.START);

    }
}